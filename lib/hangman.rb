class Hangman
  attr_reader :guesser, :referee, :board
  # attr_accessor :computer_player, :players, :guesser
  # def initialize(dictionary)
  #   @dictionary = dictionary
  # end

  def initialize(players)
    @guesser = players[:guesser]
    @referee = players[:referee]
    @board = []
  end

  # def guesser
  #   @guesser
  #   # @guesser ## how does attr_reader return the guessing player?
  #   # I thought it was simply declaring an instance var @guesser
  # end
  #
  # def referee
  #   # @referee
  #   # ditto
  # end

  # def board
  #   # @board = @dictionary
  # end

  def setup
    # how do you 'tell' the the referee/guesser to do things
    # btw since the method above are a mystery, I cant really do this part
    length = @referee.pick_secret_word
    @guesser.register_secret_length(length)
    @board = Array.new(length)
  end

  def take_turn
    #to ask guesser?
    guess = @guesser.guess(@board)
    indices = @referee.check_guess(guess)
    update_board(indices, guess)
    @guesser.handle_response(guess, indices)
  end

  def update_board(indices, letter)
    indices.each {|idx| @board[idx] = letter}
  end
end

class HumanPlayer

end

class ComputerPlayer
  # attr_reader :computer_player, :dictionary, :board
  def initialize(dictionary)
    @dictionary = dictionary
    @alphabet = ('a'..'z').to_a
  end

  def pick_secret_word
    @secret_word = @dictionary.sample
    @secret_word.length
    # dictionary[0].length
  end

  def check_guess(letter)
    i = []
    @secret_word.each_char.with_index do |char,idx|
      i << idx if char == letter
    end
    i
    # return [] if !@secret_word.include?(letter)
    # idx = []
    # @secret_word.chars.each.with_index do |ch,i|
    #    if ch == letter
    #      idx << i
    #    end
    #  end
    #  idx
  end

  def register_secret_length(length)
    # @secret_length = length

    @dictionary.select! {|word| word.length == length}
  end

  def guess(board)
    hash = Hash.new(0)
    common = @dictionary.join
    common.chars.each {|ch| hash[ch] += 1}
    hash.select {|k,v| v > 1}.keys.sample #ran it till it worked for both!
  end


  def handle_response(letter, indices)
    @dictionary.select! do |word|
        indices.all? {|i| word[i] == letter}
    end
    @dictionary.select! {|word| word.count(letter) == indices.length }
    if letter == nil
    end
  end

  def candidate_words
    @dictionary
  end

end
